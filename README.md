DKAN is a community-driven, free and open source data management platform that gives organizations and individuals ultimate freedom to publish, share and use data.

Download the code & releases from [github.com/GetDKAN/dkan](https://github.com/GetDKAN/dkan).

Maintained by CivicActions.
